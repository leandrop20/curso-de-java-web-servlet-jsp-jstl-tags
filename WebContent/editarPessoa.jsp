<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Editar Pessoa</title>
		<link rel="stylesheet" href="css/global.css"/>
		<script type="text/javascript">
			function getCities(comboStates) {
				var state_id = comboStates.options[comboStates.selectedIndex].value;
				var formCadastro = document.forms[0];
				formCadastro.action = 'main?action=montagemCadastro&isEdit=true&getCities=true&state_id=' + state_id;
				formCadastro.submit();
			}
			
			function atualizar() {
				var formCadastro = document.forms[0];
				formCadastro.action = "main?action=atualizaPessoa";
				formCadastro.submit();
			}
		</script>
	</head>
	<body>
		<jsp:include page="header.jsp" />
			<h1>Editar Pessoa</h1>
			<div class="main">
				<form method="post">
					<jsp:include page="msg.jsp" />
					<fieldset>
						<legend>Editar Pessoa</legend>
						<table cellpadding="5">
							<tr>
								<td>Nome*:</td>
								<td><input type="text" name="name" maxlength="50" value="${person != null ? person.name : param.name}" /></td>
							</tr>
							<tr>
								<td>CPF*:</td>
								<td><input type="text" name="cpf" maxlength="11" value="${person != null ? person.cpf : param.cpf}" /></td>
							</tr>
							<tr>
								<td>Data Nascimento:</td>
								<td><input type="text" name="dtBirth" maxlength="10" value="${person != null ? person.dtBirth : param.dtBirth}" /></td>
							</tr>
							<tr>
								<td>Sexo*:</td>
								<td>
									<c:set var="sexSelected" value="${param.sex != null ? param.sex : person.sex}" />
									<input type="radio" name="sex" value="M" ${sexSelected.toString() eq 'M' ? 'checked' : ''} /> Masculino
									<input type="radio" name="sex" value="F" ${sexSelected.toString() eq 'F' ? 'checked' : ''} /> Feminino
								</td>
							</tr>
							<tr>
								<td>Preferência Musical</td>
								<td>
									<c:forEach items="${musicalPrefsList}" var="musicalPref">
										<c:set var="checked" value="" />
										<c:forEach items="${paramValues['musicalPreference'] != null ? paramValues['musicalPreference'] : person.musicalPreference}" var="musicalPrefsSelected">
											<c:set var="mp" value="${musicalPrefsSelected['class'].simpleName == 'String' ? musicalPrefsSelected : musicalPrefsSelected.id}" />
											<c:if test="${musicalPref.id eq mp}">
												<c:set var="checked" value="checked" />																	
											</c:if>
										</c:forEach>
										<input type="checkbox" name="musicalPreference" value="${musicalPref.id}" ${checked} />
										${musicalPref.description}
									</c:forEach>
								</td>
							</tr>
							<tr>
								<td>Mini-biografia:</td>
								<td>
									<textarea rows="5" cols="35" name="miniBio">${person != null ? person.minBio : param.miniBio}</textarea>
								</td>
							</tr>
						</table>
						<fieldset>
							<legend>Endereço</legend>
							<table cellpadding="5">
								<tr>
									<td>UF*:</td>
									<td>
										<select id="state" name="state" onchange="getCities(this)">
											<option value="0">Selecione...</option>
											<c:set var="state_id" value="${param.state != null ? param.state : person.address.city.state.id}" />
											<c:forEach items="${statesList}" var="state">
												<c:set var="stateSelected" value="" />
												<c:if test="${state.id eq state_id}">
													<c:set var="stateSelected" value="selected" />
												</c:if>
												<option value="${state.id}" ${stateSelected}>
													${state.description}
												</option>
											</c:forEach>
										</select>
									</td>
								</tr>
								<tr>
									<td>Cidade*:</td>
									<td>
										<select name="city">
											<option value="0">Selecione...</option>
											<c:set var="city_id" value="${param.city != null ? param.city : person.address.city.id}" />
											<c:forEach items="${citiesList}" var="city">
												<c:set var="citySelected" value="" />
												<c:if test="${city.id eq city_id}">
													<c:set var="citySelected" value="selected" />
												</c:if>
												<option value="${city.id}" ${citySelected}>
													${city.description}
												</option>
											</c:forEach>
										</select>
									</td>
								</tr>
								<tr>
									<td>Logradouro*:</td>
									<td>
										<input type="text" name="logradouro" maxlength="50" value="${person != null ? person.address.logradouro : param.logradouro}" />
									</td>
								</tr>
							</table>
						</fieldset>
					</fieldset>
					<span>* Campos Obrigatórios</span>
					<input type="reset" value="Limpar" />
					<input type="hidden" name="address_id" value="${ person != null ? person.address.id : param.address_id }" />
					<input type="hidden" name="id" value="${ person != null ? person.id : param.id }" />
					<input type="button" value="Atualizar" onclick="atualizar()" />
				</form>
			</div>
		<jsp:include page="footer.jsp" />
	</body>
</html>