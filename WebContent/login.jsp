<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Login - CRUD WEB</title>
		<link rel="stylesheet" href="css/global.css"/>
		<link rel="stylesheet" href="css/login.css"/>
	</head>
	<body>
		<form method="post" id="login_form" action="main?action=login">
			<jsp:include page="msg.jsp" />
			<fieldset id="fieldset_login">
				<legend>Login do Sistema</legend>
				<div class="field">
					<label for="Login">Login</label>
					<input name="login" type="text" id="Login" maxLength="15" value="${param.login}" />
				</div>
				<div class="field">
					<label for="Password">Senha</label>
					<input name="password" type="password" id="Password" maxLength="15" value="${param.password}" />
				</div>
				<div class="field">
					<input type="submit" value="Entrar" />
				</div>
				<div class="field">
					<a href="">Esqueci a senha</a>
				</div>
			</fieldset>
		</form>
	</body>
</html>