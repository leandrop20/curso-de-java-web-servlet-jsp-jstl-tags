<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Cadastros</title>
		<link rel="stylesheet" href="css/global.css"/>
		<script type="text/javascript">
			function getCities(comboStates) {
				var formCadastro = document.forms[0];
				formCadastro.action = "main?action=montagemCadastro";
				formCadastro.submit();
			}
			
			function cadastrar() {
				var formCadastro = document.forms[0];
				formCadastro.action = "main?action=cadastroPessoa";
				formCadastro.submit();
			}
		</script>
	</head>
	<body>
		<jsp:include page="header.jsp" />
			<h1>Cadastros</h1>
			<div class="main">
				<form action="main?action=cadastroPessoa" method="post">
					<jsp:include page="msg.jsp" />
					<fieldset>
						<legend>Cadastro de Pessoa</legend>
						<table cellpadding="5">
							<tr>
								<td>Nome*:</td>
								<td><input type="text" name="name" maxlength="50" value="${param.name}" /></td>
							</tr>
							<tr>
								<td>CPF*:</td>
								<td><input type="text" name="cpf" maxlength="11" value="${param.cpf}" /></td>
							</tr>
							<tr>
								<td>Data Nascimento:</td>
								<td><input type="text" name="dtBirth" maxlength="10" value="${param.dtBirth}" /></td>
							</tr>
							<tr>
								<td>Sexo*:</td>
								<td>
									<input type="radio" name="sex" value="M" ${param.sex eq 'M' ? 'checked="checked"' : ''} /> Masculino
									<input type="radio" name="sex" value="F" ${param.sex eq 'F' ? 'checked="checked"' : ''} /> Feminino
								</td>
							</tr>
							<tr>
								<td>Preferência Musical</td>
								<td>
									<c:if test="${musicalPrefsList != null}">
										<c:forEach items="${musicalPrefsList}" var="musicalPref">
											<c:set var="checked" value="" />
											<c:forEach items="${paramValues['musicalPreference']}" var="musicalPrefsSelected">
												<c:if test="${musicalPref.id eq musicalPrefsSelected}">
													<c:set var="checked" value="checked" />
												</c:if>
											</c:forEach>
											<input type="checkbox" name="musicalPreference" value="${musicalPref.id}" ${checked} />
											${musicalPref.description}
										</c:forEach>
									</c:if>
								</td>
							</tr>
							<tr>
								<td>Mini-biografia:</td>
								<td>
									<textarea rows="5" cols="35" name="miniBio">${param.miniBio}</textarea>
								</td>
							</tr>
						</table>
						<fieldset>
							<legend>Endereço</legend>
							<table cellpadding="5">
								<tr>
									<td>UF*:</td>
									<td>
										<select id="state" name="state" onchange="getCities(this)">
											<option value="0">Selecione...</option>
											<c:if test="${statesList != null}">
												<c:set var="state_id" value="${param.state}" />
												<c:forEach items="${statesList}" var="state">
													<c:set var="stateSelected" value="" />
													<c:if test="${state.id eq state_id}">
														<c:set var="stateSelected" value="selected" />
													</c:if>
													<option value="${state.id}" ${stateSelected}>
														${state.description}
													</option>
												</c:forEach>
											</c:if>														
										</select>
									</td>
								</tr>
								<tr>
									<td>Cidade*:</td>
									<td>
										<select name="city">
											<option value="0">Selecione...</option>
											<c:if test="${citiesList != null}">
												<c:set var="city_id" value="${param.city}" />
												<c:forEach items="${citiesList}" var="city">
													<c:set var="citySelected" value="" />
													<c:if test="${city.id eq city_id}">
														<c:set var="citySelected" value="selected" />
													</c:if>
													<option value="${city.id}" ${citySelected}>
														${city.description}
													</option>
												</c:forEach>
											</c:if>
										</select>
									</td>
								</tr>
								<tr>
									<td>Logradouro*:</td>
									<td>
										<input type="text" name="logradouro" maxlength="50" value="${param.logradouro}" />
									</td>
								</tr>
							</table>
						</fieldset>
					</fieldset>
					<span>* Campos Obrigatórios</span>
					<input type="reset" value="Limpar" />
					<input type="button" value="Cadastrar" onclick="cadastrar()" />
				</form>
			</div>
		<jsp:include page="footer.jsp" />
	</body>
</html>