<div class="header">
	<div class="logo">
		<a href="index.jsp">
			<img alt="Logo Devmedia" src="img/logo-devmedia.png" />
		</a>
	</div>
	<ul>
		<li><a href="main?action=index" class="${param.action eq 'index'?'selected':''}">Home</a></li>
		<li><a href="main?action=montagemCadastro" class="${param.action eq 'cadastro'?'selected':''}">Cadastros</a></li>
		<li><a href="main?action=consultaPessoas" class="${param.action eq 'consulta'?'selected':''}">Consultas</a></li>
		<li><a href="main?action=logout" class="${param.action eq 'sair'?'selected':''}">Sair</a></li>
	</ul>
	<div class="wellcome">
		Bem-vindo(a), <b style="color:gray">${sessionScope.user.login}</b>!
	</div>
</div>