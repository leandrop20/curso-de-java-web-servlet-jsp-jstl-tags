<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="disp" uri="http://displaytag.sf.net" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Consulta Pessoas</title>
		<link rel="stylesheet" href="css/global.css"/>
		<link href="css/displaytag.css" rel="stylesheet" type="text/css"/>
		<link href="css/screen.css" rel="stylesheet" type="text/css"/>
		<link href="css/site.css" rel="stylesheet" type="text/css"/>
		<script>
			function getCities()
			{
				document.forms[0].action = "main?action=consultaPessoas";
				document.forms[0].submit();
			}
		</script>
	</head>
	<body>
		<jsp:include page="header.jsp" />
		<div class="main">
			<jsp:include page="msg.jsp" />
			<fieldset>
				<legend>Consulta</legend>
				<h1>Consulta</h1>
				
				<fieldset style="margin-bottom: 20px">
					<legend>Selecione os par�metros</legend>
					<form action="main?action=filtrarPessoas" method="post">
						<fieldset style="width:48%; float:left;">
							<legend>Dados Pessoais</legend>
							<table cellpadding="5">
								<tr>
									<td>Nome</td>
									<td><input type="text" name="name" maxlength="45" value="${param.name}" /></td>
								</tr>
								<tr>
									<td>CPF</td>
									<td><input type="text" name="cpf" maxlength="11" value="${param.cpf}" /></td>
								</tr>
								<tr>
									<td>Data Nascimento</td>
									<td><input type="text" name="dtBirth" maxlength="10" value="${param.dtBirth}" /></td>
								</tr>
								<tr>
									<td>Sexo</td>
									<td>
										<input type="radio" name="sex" value="M" ${param.sex eq 'M' ? 'checked' : ''} /> Masculino
										<input type="radio" name="sex" value="F" ${param.sex eq 'F' ? 'checked' : ''} /> Feminino
									</td>
								</tr>
								<tr>
									<td>Prefer�ncia Musical</td>
									<td>
										<c:forEach items="${mPrefsList}" var="mPref">
											<c:set var="checkedPref" value="" />	
											<c:forEach items="${paramValues['musicalPreference']}" var="mPrefSel">
												<c:if test="${mPref.id eq mPrefSel}">
													<c:set var="checkedPref" value="checked" />	
												</c:if>
											</c:forEach>
											<input type="checkbox" name="musicalPreference" value="${mPref.id}" ${checkedPref} />
											${mPref.description}
										</c:forEach>
									</td>
								</tr>
							</table>
						</fieldset>
						<fieldset>
							<legend>Endere�o</legend>
							<table cellpadding="5">
								<tr>
									<td>UF:</td>
									<td>
										<select id="state" name="state" onchange="getCities()">
											<option value="0">Selecione...</option>
											<c:forEach items="${statesList}" var="state">
												<c:set var="stateSelected" value="" />
												<c:if test="${param.state != null && param.state eq state.id}">
													<c:set var="stateSelected" value="selected" />
												</c:if>
												<option value="${state.id}" ${stateSelected}>
													${state.description}
												</option>
											</c:forEach>
										</select>
									</td>
								</tr>
								<tr>
									<td>Cidade:</td>
									<td>
										<select name="city">
											<option value="0">Selecione...</option>
											<c:if test="${param.state != null}">
												<c:forEach items="${citiesList}" var="city">
													<c:set var="citySelected" value="" />
													<c:if test="${param.city != null && param.city eq city.id}">
														<c:set var="citySelected" value="selected" />
													</c:if>
													<option value="${city.id}" ${citySelected}>
														${city.description}
													</option>
												</c:forEach>
											</c:if>
										</select>
									</td>
								</tr>
								<tr>
									<td>Logradouro</td>
									<td>
										<input type="text" name="logradouro" maxlength="50" value="${param.logradouro}" />
									</td>
								</tr>
							</table>
						</fieldset>
						<div style="clear:both;"></div>
						<input type="submit" value="Consultar" />
						<input type="button" value="Limpar" onclick="location.href='main?action=consultaPessoas'" />
					</form>
				</fieldset>
				<table width="100%" border="1" cellspacing="0" cellpadding="5">
					<thead>
						<tr>
							<th>Id</th>
							<th>Nome</th>
							<th>CPF</th>
							<th>Sexo</th>
							<th>Dt Nasc.</th>
							<th>Endere�o</th>
							<th>Cidade</th>
							<th>UF</th>
							<th colspan="3">A��es</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${personsList}" var="person">
							<tr>
								<td class="alignCenter">${person.id}</td>
								<td class="alignLeft">${person.name}</td>
								<td class="alignCenter">${person.sex}</td>
								<td class="alignCenter">${person.cpf}</td>
								<td class="alignCenter">${person.dtBirth}</td>
								<td class="alignLeft">${person.address.logradouro}</td>
								<td class="alignLeft">${person.address.city.description}</td>
								<td class="alignLeft">${person.address.city.state.initial}</td>
								<td class="alignCenter">
									<c:set var="prefs" value="Sem Prefer�ncias" />
									<c:forEach items="${person.musicalPreference}" var="pref" varStatus="status">
										<c:set var="prefs" value="${status.first ? ' ' : prefs} [${pref.description}]" />
									</c:forEach>
									<a href="javascript:void(0)" title="Detalhes" onclick="alert('${prefs}');">
										<img alt="Prefer�ncias Musicais" src="img/info.png" />
									</a>
								</td>
								<td class="alignCenter">
									<a href="main?action=editarPessoa&id=${person.id}" title="Editar">
										<img alt="Edi��o de Pessoa" src="img/edit.png" />
									</a>
								</td>
								<td class="alignCenter">
									<a href="main?action=removerPessoa&id=${person.id}" title="Deletar">
										<img alt="Remo��o de Pessoa" src="img/delete.png" />
									</a>
								</td>
							</tr>
						</c:forEach>
						<c:if test="${empty personsList}">
							<tr style="color:red; text-align:center; font-size:15pt">
								<td colspan="9">Nenhum registro encontrado!</td>
							</tr>
						</c:if>
					</tbody>
				</table>
			</fieldset>
			<fieldset>
				<legend>Lista de Pessoas</legend>
				<div id="tab1" class="tab_content" style="width:100%; display:block;">
					<h3>Lista de Pessoas</h3>
					<disp:table name="personsList" pagesize="3" style="width:80%" uid="list"
						sort="list" decorator="br.edu.devmedia.crud.decorator.PersonDecorator">
						<disp:column property="id" class="sortable" title="Id" sortable="true"></disp:column>
						<disp:column property="name" class="sortable" title="Nome" sortable="true"></disp:column>
						<disp:column property="cpf" class="sortable" title="CPF" sortable="true"></disp:column>
						<disp:column property="sex" class="sortable" title="Sexo" sortable="true"></disp:column>
						<disp:column property="dtBirth" class="sortable" title="Dt Nasc." sortable="true"></disp:column>
						<disp:column property="address.logradouro" class="sortable" title="Endere�o" sortable="true"></disp:column>
						<disp:column property="address.city.description" class="sortable" title="Cidade" sortable="true"></disp:column>
						<disp:column property="address.city.state.initial" class="sortable" title="UF" sortable="true"></disp:column>
						<disp:column property="musicalPreferences" class="sortable" title="" sortable="false"></disp:column>
						<disp:column property="edit" title="Editar"></disp:column>
						<disp:column property="delete" title="Deletar"></disp:column>
					</disp:table>
				</div>
			</fieldset>
		</div>
		<jsp:include page="footer.jsp" />
	</body>
</html>