package br.edu.devmedia.crud.command;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import br.edu.devmedia.crud.bo.PersonsBO;
import br.edu.devmedia.crud.dto.AddressDTO;
import br.edu.devmedia.crud.dto.CityDTO;
import br.edu.devmedia.crud.dto.MusicalPreferenceDTO;
import br.edu.devmedia.crud.dto.PersonDTO;
import br.edu.devmedia.crud.dto.StateDTO;
import br.edu.devmedia.crud.util.ConstantMessages;

public class UpdatePersonCommand implements Command {
	
	private String next;
	private PersonsBO personsBO;
	
	@Override
	public String execute(HttpServletRequest req) {
		Integer address_id = Integer.parseInt(req.getParameter("address_id"));
		Integer id = Integer.parseInt(req.getParameter("id"));
		next = "main?action=editarPessoa&id=" + id;
		personsBO = new PersonsBO();
		
		String name = req.getParameter("name");
		String cpf = req.getParameter("cpf");
		String dtBirth = req.getParameter("dtBirth");
		String sex = req.getParameter("sex");
		String miniBio = req.getParameter("miniBio");
		String stateID = req.getParameter("state");
		String cityID = req.getParameter("city");
		String logradouro = req.getParameter("logradouro");
		
		String[] musicalPreferenceList = req.getParameterValues("musicalPreference");
		List<MusicalPreferenceDTO> listPrefs = new ArrayList<MusicalPreferenceDTO>();
		if (musicalPreferenceList != null) {
			for (String pref : musicalPreferenceList) {
				MusicalPreferenceDTO musicalPreference = new MusicalPreferenceDTO();
				musicalPreference.setId(Integer.parseInt(pref));
				
				listPrefs.add(musicalPreference);
			}
		}
		
		try {
			PersonDTO personDTO = new PersonDTO();
			personDTO.setId(id);
			personDTO.setName(name);
			personDTO.setCpf(cpf);
			personDTO.setDtBirth(dtBirth);
			personDTO.setSex(sex != null ? sex.charAt(0) : ' ');
			personDTO.setMinBio(miniBio);
			personDTO.setMusicalPreference(listPrefs);
			
			AddressDTO addressDTO = new AddressDTO();
			addressDTO.setId(address_id);
			addressDTO.setLogradouro(logradouro);
			
			CityDTO cityDTO = new CityDTO();
			cityDTO.setId(cityID != null ? Integer.parseInt(cityID) : null);
			
			StateDTO stateDTO = new StateDTO();
			stateDTO.setId(stateID != null ? Integer.parseInt(stateID) : null);
			
			cityDTO.setState(stateDTO);
			addressDTO.setCity(cityDTO);
			personDTO.setAddress(addressDTO);
			
			boolean isValid = personsBO.validatePerson(personDTO);
			if (!isValid) {
				req.setAttribute("msgErro", ConstantMessages.MSG_ERR_PERSON_INVALIDO);
			} else {
				personsBO.edit(personDTO);
				next = "main?action=consultaPessoas";
				req.setAttribute("msgSucesso", ConstantMessages.MSG_SUC_EDIT_PERSON);
			}
			
		} catch (Exception e) {
			req.setAttribute("msgErro", e.getMessage());
		}
		
		return next;
	}
	
}