package br.edu.devmedia.crud.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import br.edu.devmedia.crud.bo.CitiesBO;
import br.edu.devmedia.crud.bo.MusicalPreferencesBO;
import br.edu.devmedia.crud.bo.PersonsBO;
import br.edu.devmedia.crud.bo.StatesBO;
import br.edu.devmedia.crud.dto.CityDTO;
import br.edu.devmedia.crud.dto.MusicalPreferenceDTO;
import br.edu.devmedia.crud.dto.PersonDTO;
import br.edu.devmedia.crud.dto.StateDTO;

public class ListPersonsCommand implements Command {
	
	private String next;
	private PersonsBO personBO;
	private MusicalPreferencesBO mPrefsBO;
	private StatesBO statesBO;
	private CitiesBO citiesBO;
	
	@Override
	public String execute(HttpServletRequest req) {
		next = "consultaPessoas.jsp";
		personBO = new PersonsBO();
		mPrefsBO = new MusicalPreferencesBO();
		statesBO = new StatesBO();
		citiesBO = new CitiesBO();
		
		try {
			List<PersonDTO> personsList = personBO.getList();
			req.setAttribute("personsList", personsList);
			
			List<MusicalPreferenceDTO> mPrefs = mPrefsBO.getList();
			req.setAttribute("mPrefsList", mPrefs);
			
			List<StateDTO> statesList = statesBO.getList();
			req.setAttribute("statesList", statesList);
			
			if (req.getParameter("state") != null) {
				Integer state_id = Integer.parseInt(req.getParameter("state"));
				List<CityDTO> citiesList = citiesBO.getListByStateID(state_id);
				req.setAttribute("citiesList", citiesList);
			}
		} catch (Exception e) {
			req.setAttribute("msgErro", e.getMessage());
		}
		
		return next;
	}
	
}
