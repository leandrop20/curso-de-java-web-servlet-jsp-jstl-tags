package br.edu.devmedia.crud.command;

import javax.servlet.http.HttpServletRequest;

public class LogoutCommand implements Command {
	
	public String execute(HttpServletRequest req) {
		req.getSession().invalidate();
		req.setAttribute("msgSucesso", "Volte Sempre!");
		return "login.jsp";
	}
	
}