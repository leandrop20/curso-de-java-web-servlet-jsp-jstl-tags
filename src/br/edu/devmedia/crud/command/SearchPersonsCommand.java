package br.edu.devmedia.crud.command;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import br.edu.devmedia.crud.bo.CitiesBO;
import br.edu.devmedia.crud.bo.MusicalPreferencesBO;
import br.edu.devmedia.crud.bo.PersonsBO;
import br.edu.devmedia.crud.bo.StatesBO;
import br.edu.devmedia.crud.dto.AddressDTO;
import br.edu.devmedia.crud.dto.CityDTO;
import br.edu.devmedia.crud.dto.MusicalPreferenceDTO;
import br.edu.devmedia.crud.dto.PersonDTO;
import br.edu.devmedia.crud.dto.StateDTO;

public class SearchPersonsCommand implements Command {
	
	private String next;
	private PersonsBO personBO;
	private MusicalPreferencesBO mPrefsBO;
	private StatesBO statesBO;
	private CitiesBO citiesBO;
	
	@Override
	public String execute(HttpServletRequest req) {
		next = "consultaPessoas.jsp";
		personBO = new PersonsBO();
		mPrefsBO = new MusicalPreferencesBO();
		statesBO = new StatesBO();
		citiesBO = new CitiesBO();
		
		try {
			String name = req.getParameter("name");
			String cpf = req.getParameter("cpf");
			String dtBirth = req.getParameter("dtBirth");
			String sex = req.getParameter("sex");
			Integer state_id = Integer.parseInt(req.getParameter("state"));
			Integer city_id = Integer.parseInt(req.getParameter("city"));
			String logradouro = req.getParameter("logradouro");
			
			PersonDTO personDTO = new PersonDTO();
			personDTO.setName(name);
			personDTO.setCpf(cpf);
			personDTO.setDtBirth(dtBirth);
			personDTO.setSex(sex != null ? sex.charAt(0) : ' ');
			
			AddressDTO address = new AddressDTO();
			address.setLogradouro(logradouro);
			
			CityDTO cityDTO = new CityDTO();
			cityDTO.setId(city_id);
			
			StateDTO stateDTO = new StateDTO();
			stateDTO.setId(state_id);
			cityDTO.setState(stateDTO);
			
			address.setCity(cityDTO);
			personDTO.setAddress(address);
			
			if (req.getParameter("musicalPreference") != null) {
				String[] musicalPreference = req.getParameterValues("musicalPreference");
				List<MusicalPreferenceDTO> prefsSelected = new ArrayList<>();
				for (String pref : musicalPreference) {
					MusicalPreferenceDTO mPref = new MusicalPreferenceDTO();
					mPref.setId(Integer.parseInt(pref));
					
					prefsSelected.add(mPref);
				}
				personDTO.setMusicalPreference(prefsSelected);
			}
			
			List<PersonDTO> personsList = personBO.getListBySearch(personDTO);
			req.setAttribute("personsList", personsList);
			
			List<MusicalPreferenceDTO> mPrefsList = mPrefsBO.getList();
			req.setAttribute("mPrefsList", mPrefsList);
			
			List<StateDTO> statesList = statesBO.getList();
			req.setAttribute("statesList", statesList);
			
			if (req.getParameter("state") != null) {
				List<CityDTO> citiesList = citiesBO.getListByStateID(state_id);
				req.setAttribute("citiesList", citiesList);
			}
		} catch (Exception e) {
			req.setAttribute("msgErro", e.getMessage());
		}
		
		return next;
	}
	
}
