package br.edu.devmedia.crud.command;

import javax.servlet.http.HttpServletRequest;

import br.edu.devmedia.crud.bo.UsersBO;
import br.edu.devmedia.crud.dto.UserDTO;
import br.edu.devmedia.crud.exception.BusinessException;

public class LoginCommand implements Command {

	private UsersBO usersBO;
	private String next;
	
	public String execute(HttpServletRequest req) {
		next = "login.jsp";
		usersBO = new UsersBO();
		
		String login = req.getParameter("login");
		String password = req.getParameter("password");
		
		UserDTO userDTO = new UserDTO();
		userDTO.setLogin(login);
		userDTO.setPassword(password);
		
		try {
			if (usersBO.authUser(userDTO)) {
				req.getSession().setAttribute("user", userDTO);
				next = "index.jsp";
			}
		} catch (BusinessException e) {
			req.setAttribute("msgErro", e.getMessage());
		}
		return next;
	}
	
}
