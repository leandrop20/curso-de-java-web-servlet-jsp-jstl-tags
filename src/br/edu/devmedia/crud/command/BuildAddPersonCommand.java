package br.edu.devmedia.crud.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import br.edu.devmedia.crud.bo.CitiesBO;
import br.edu.devmedia.crud.bo.MusicalPreferencesBO;
import br.edu.devmedia.crud.bo.StatesBO;
import br.edu.devmedia.crud.dto.CityDTO;
import br.edu.devmedia.crud.dto.MusicalPreferenceDTO;
import br.edu.devmedia.crud.dto.StateDTO;

public class BuildAddPersonCommand implements Command {
	
	private String next;
	private StatesBO statesBO;
	private CitiesBO citiesBO;
	private MusicalPreferencesBO musicalPreferencesBO;
	
	@Override
	public String execute(HttpServletRequest req) {
		statesBO = new StatesBO();
		citiesBO = new CitiesBO();
		musicalPreferencesBO = new MusicalPreferencesBO();
		
		String isEdit = req.getParameter("isEdit");
		if (isEdit != null && !"".equals(isEdit)) {
			next = "editarPessoa.jsp";
		} else {
			next = "cadastroPessoa.jsp";
		}
		
		List<StateDTO> statesList = this.statesBO.getList();
		req.setAttribute("statesList", statesList);
		
		List<MusicalPreferenceDTO> musicalPrefs = this.musicalPreferencesBO.getList();
		req.setAttribute("musicalPrefsList", musicalPrefs);
		
		if (req.getParameter("state") != null) {
			int state_id = Integer.parseInt(req.getParameter("state"));
			List<CityDTO> citiesList = this.citiesBO.getListByStateID(state_id);
			req.setAttribute("citiesList", citiesList);
		}
		
		return next;
	}
	
}
