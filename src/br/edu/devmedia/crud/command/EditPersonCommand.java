package br.edu.devmedia.crud.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import br.edu.devmedia.crud.bo.CitiesBO;
import br.edu.devmedia.crud.bo.MusicalPreferencesBO;
import br.edu.devmedia.crud.bo.PersonsBO;
import br.edu.devmedia.crud.bo.StatesBO;
import br.edu.devmedia.crud.dto.CityDTO;
import br.edu.devmedia.crud.dto.MusicalPreferenceDTO;
import br.edu.devmedia.crud.dto.PersonDTO;
import br.edu.devmedia.crud.dto.StateDTO;

public class EditPersonCommand implements Command {
	
	private String next;
	private PersonsBO personsBO;
	private StatesBO statesBO;
	private CitiesBO citiesBO;
	private MusicalPreferencesBO musicalPreferencesBO;
	
	@Override
	public String execute(HttpServletRequest req) {
		next = "editarPessoa.jsp";
		personsBO = new PersonsBO();
		statesBO = new StatesBO();
		citiesBO = new CitiesBO();
		musicalPreferencesBO = new MusicalPreferencesBO();
		
		try {
			Integer person_id = Integer.parseInt(req.getParameter("id"));
			PersonDTO person = personsBO.getByID(person_id);
			req.setAttribute("person", person);
			
			List<StateDTO> statesList = statesBO.getList();
			req.setAttribute("statesList", statesList);
			
			List<MusicalPreferenceDTO> musicalPrefs = musicalPreferencesBO.getList();
			req.setAttribute("musicalPrefsList", musicalPrefs);
			
			Integer state_id = null;
			if (req.getParameter("state") != null) {
				state_id = Integer.parseInt(req.getParameter("state"));
			} else {
				state_id = person.getAddress().getCity().getState().getId();
			}
			List<CityDTO> citiesList = citiesBO.getListByStateID(state_id);
			req.setAttribute("citiesList", citiesList);
		} catch (Exception e) {
			req.setAttribute("msgErro", e.getMessage());
		}
		
		return next;
	}
	
}