package br.edu.devmedia.crud.command;

import javax.servlet.http.HttpServletRequest;

import br.edu.devmedia.crud.bo.PersonsBO;

public class deletePersonCommand implements Command {
	
	private String next;
	private PersonsBO personsBO;
	
	@Override
	public String execute(HttpServletRequest req) {
		next = "main?action=consultaPessoas";
		this.personsBO = new PersonsBO();
		
		try {
			Integer person_id = Integer.parseInt(req.getParameter("id"));
			personsBO.delete(person_id);
			req.setAttribute("msgSucesso", "Pessoa removida com sucesso!");
		} catch (Exception e) {
			req.setAttribute("msgErro", e.getMessage());
		}
		
		return next;
	}
	
}