package br.edu.devmedia.crud.cewolf;

import java.util.Date;
import java.util.Map;

import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import de.laures.cewolf.DatasetProduceException;
import de.laures.cewolf.DatasetProducer;
import de.laures.cewolf.links.CategoryItemLinkGenerator;
import de.laures.cewolf.tooltips.CategoryToolTipGenerator;

public class ViewContDados implements DatasetProducer, CategoryToolTipGenerator, CategoryItemLinkGenerator {

	private static final long serialVersionUID = 1L;
	
	private final static String[] days = {"seg", "ter", "qua", "qui", "sex", "sab"};
	private final static String[] newspapers = {"O Povo", "Wall Street", "Folha SP", "O Globo"};

	@Override
	public String getProducerId() {
		return "ABC";
	}

	@Override
	public boolean hasExpired(Map map, Date desde) {
		return (System.currentTimeMillis() - desde.getTime()) > 5000;
	}

	@Override
	public Object produceDataset(Map<String, Object> arg0) throws DatasetProduceException {
		DefaultCategoryDataset dataset = new DefaultCategoryDataset() {
			private static final long serialVersionUID = 1L;

			@Override
			protected void finalize() throws Throwable {
				super.finalize();
				System.out.println("Finalizou");
			}
		};
		
		for (int i = 0; i < newspapers.length; i++) {
			int lastY = (int) (Math.random() * 1000 + 2000);
			for (int j = 0; j < days.length; j++) {
				final int y = lastY + (int) (Math.random() * 800 - 100);
				lastY = y;
				dataset.addValue(y, newspapers[i], days[j]);
			}
		}
		
		return dataset;
	}

	@Override
	public String generateToolTip(CategoryDataset arg0, int name, int arg2) {
		return newspapers[name];
	}

	@Override
	public String generateLink(Object arg0, int name, Object arg2) {
		return newspapers[name];
	}
	
}
