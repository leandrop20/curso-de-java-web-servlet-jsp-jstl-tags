package br.edu.devmedia.crud.bo;

import java.util.List;

import br.edu.devmedia.crud.dao.CitiesDAO;
import br.edu.devmedia.crud.dto.CityDTO;
import br.edu.devmedia.crud.exception.PersistenceException;

public class CitiesBO {

	private CitiesDAO citiesDAO;
	
	public CitiesBO()
	{
		citiesDAO = new CitiesDAO();
	}
	
	/**
	 * 
	 * @return
	 */
	public List<CityDTO> getListByStateID(Integer id)
	{
		List<CityDTO> cities = null;
		try {
			cities = citiesDAO.getListByStateID(id);
		} catch (PersistenceException e) {
			e.printStackTrace();
		}
		return cities;
	} 
	
}
