package br.edu.devmedia.crud.bo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.edu.devmedia.crud.dao.PersonsDAO;
import br.edu.devmedia.crud.dto.CityDTO;
import br.edu.devmedia.crud.dto.PersonDTO;
import br.edu.devmedia.crud.exception.BusinessException;
import br.edu.devmedia.crud.exception.PersistenceException;
import br.edu.devmedia.crud.util.ConstantMessages;
import br.edu.devmedia.crud.validator.CPFValidator;
import br.edu.devmedia.crud.validator.DataValidator;

public class PersonsBO {
	
	private PersonsDAO personsDAO;
	
	public PersonsBO()
	{
		personsDAO = new PersonsDAO();
	}
	
	/**
	 * 
	 * @param personDTO
	 * @return
	 * @throws BusinessException
	 */
	public boolean validatePerson(PersonDTO personDTO) throws BusinessException {
		boolean isValid = false;
		
		try {
			if (personDTO.getName() == null || "".equals(personDTO.getName())) {
				throw new BusinessException(ConstantMessages.MSG_ERR_CAMPO_OBRIGATORIO.replace("?", "Nome"));
			}
			
			Map<String, Object> valores = new HashMap<>();
			valores.put("CPF", personDTO.getCpf());
			if (new CPFValidator().validar(valores)) {
				isValid = true;
			}
			
			valores = new HashMap<>();
			valores.put("Data Nasc.", personDTO.getDtBirth());
			if (new DataValidator().validar(valores)) {
				isValid = true;
			}
			
			if (personDTO.getSex() == ' ') {
				throw new BusinessException(ConstantMessages.MSG_ERR_CAMPO_OBRIGATORIO.replace("?", "Sexo"));
			}
			
			CityDTO city = personDTO.getAddress().getCity();
			if (city.getState().getId() == null || city.getState().getId() == 0) {
				throw new BusinessException(ConstantMessages.MSG_ERR_CAMPO_OBRIGATORIO.replace("?", "Estado"));
			}
			
			if (city.getId() == null || city.getId() == 0) {
				throw new BusinessException(ConstantMessages.MSG_ERR_CAMPO_OBRIGATORIO.replace("?", "Cidade"));
			}
			
			if (personDTO.getAddress().getLogradouro() == null || "".equals(personDTO.getAddress().getLogradouro())) {
				throw new BusinessException(ConstantMessages.MSG_ERR_CAMPO_OBRIGATORIO.replace("?", "Logradouro"));
			}
			
			if (!isValid) {
				throw new BusinessException(ConstantMessages.MSG_ERR_USER_INVALIDO);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new BusinessException(e);
		}
		
		return isValid;
	}
	
	/**
	 * 
	 * @return
	 */
	public List<PersonDTO> getList()
	{
		List<PersonDTO> persons = null;
		try {
			persons = personsDAO.getList();
		} catch (PersistenceException e) {
			e.printStackTrace();
		}
		return persons;
	}
	
	/**
	 * 
	 * @param personDTO
	 * @return
	 */
	public List<PersonDTO> getListBySearch(PersonDTO personDTO)
	{
		List<PersonDTO> persons = null;
		
		try {
			persons = personsDAO.getListBySearch(personDTO);
		} catch (PersistenceException e) {
			e.printStackTrace();
		}
		
		return persons;
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 * @throws BusinessException
	 */
	public PersonDTO getByID(Integer id) throws BusinessException
	{
		try {
			return personsDAO.getByID(id);
		} catch (PersistenceException e) {
			e.printStackTrace();
			throw new BusinessException(e);
		}
	}
	
	/**
	 * 
	 * @param personDTO
	 * @throws BusinessException
	 */
	public void add(PersonDTO personDTO) throws BusinessException
	{
		try {
			personsDAO.add(personDTO);
		} catch (PersistenceException e) {
			e.printStackTrace();
			throw new BusinessException(e);
		}
	}
	
	/**
	 * 
	 * @param personDTO
	 * @throws BusinessException
	 */
	public void edit(PersonDTO personDTO) throws BusinessException
	{
		try {
			personsDAO.edit(personDTO);
		} catch (PersistenceException e) {
			e.printStackTrace();
			throw new BusinessException(e);
		}
	}
	
	/**
	 * 
	 * @param personDTO
	 * @throws BusinessException
	 */
	public void delete(Integer id) throws BusinessException
	{
		try {
			personsDAO.delete(id);
		} catch (PersistenceException e) {
			e.printStackTrace();
			throw new BusinessException(e);
		}
	}
	
}
