package br.edu.devmedia.crud.bo;

import java.util.List;

import br.edu.devmedia.crud.dao.StatesDAO;
import br.edu.devmedia.crud.dto.StateDTO;
import br.edu.devmedia.crud.exception.PersistenceException;

public class StatesBO {
	
	private StatesDAO statesDAO;
	
	public StatesBO()
	{
		statesDAO = new StatesDAO();
	}
	
	/**
	 * 
	 * @return
	 */
	public List<StateDTO> getList()
	{
		List<StateDTO> states = null;
		try {
			states = statesDAO.getList();
		} catch (PersistenceException e) {
			e.printStackTrace();
		}
		return states;
	}
	
}
