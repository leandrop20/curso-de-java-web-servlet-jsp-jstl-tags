package br.edu.devmedia.crud.bo;

import java.util.HashMap;
import java.util.Map;

import br.edu.devmedia.crud.dao.UsersDAO;
import br.edu.devmedia.crud.dto.UserDTO;
import br.edu.devmedia.crud.exception.BusinessException;
import br.edu.devmedia.crud.util.ConstantMessages;
import br.edu.devmedia.crud.validator.LoginValidator;

public class UsersBO {

	public boolean authUser(UserDTO userDTO) throws BusinessException {
		boolean isAuth = false;
		
		try {
			Map<String, Object> valores = new HashMap<>();
			valores.put("login", userDTO.getLogin());
			valores.put("password", userDTO.getPassword());
			if (new LoginValidator().validar(valores)) {
				isAuth = true;
			}
			
			UsersDAO usersDAO = new UsersDAO();
			isAuth = usersDAO.authUser(userDTO);
			if (!isAuth) {
				throw new BusinessException(ConstantMessages.MSG_ERR_USER_INVALIDO);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new BusinessException(e);
		}
		
		return isAuth;
	}
	
}
