package br.edu.devmedia.crud.bo;

import java.util.List;

import br.edu.devmedia.crud.dao.MusicalPreferencesDAO;
import br.edu.devmedia.crud.dto.MusicalPreferenceDTO;
import br.edu.devmedia.crud.exception.PersistenceException;

public class MusicalPreferencesBO {

	private MusicalPreferencesDAO musicalPreferencesDAO;
	
	public MusicalPreferencesBO()
	{
		musicalPreferencesDAO = new MusicalPreferencesDAO();
	}
	
	/**
	 * 
	 * @return
	 */
	public List<MusicalPreferenceDTO> getList()
	{
		List<MusicalPreferenceDTO> musicalPreferences = null;
		try {
			musicalPreferences = musicalPreferencesDAO.getList();
		} catch (PersistenceException e) {
			e.printStackTrace();
		}
		return musicalPreferences;
	}
	
}
