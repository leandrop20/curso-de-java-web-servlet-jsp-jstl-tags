package br.edu.devmedia.crud.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.edu.devmedia.crud.command.BuildAddPersonCommand;
import br.edu.devmedia.crud.command.deletePersonCommand;
import br.edu.devmedia.crud.command.UpdatePersonCommand;
import br.edu.devmedia.crud.command.AddPersonCommand;
import br.edu.devmedia.crud.command.Command;
import br.edu.devmedia.crud.command.ListPersonsCommand;
import br.edu.devmedia.crud.command.EditPersonCommand;
import br.edu.devmedia.crud.command.IndexCommand;
import br.edu.devmedia.crud.command.LoginCommand;
import br.edu.devmedia.crud.command.LogoutCommand;
import br.edu.devmedia.crud.command.SearchPersonsCommand;

/**
 * Servlet implementation class MainServlet
 */
@WebServlet("/main")
public class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	private Map<String, Command> commands = new HashMap<String, Command>();
	
	@Override
	public void init() throws ServletException {
		commands.put("login", new LoginCommand());
		commands.put("logout", new LogoutCommand());
		commands.put("index", new IndexCommand());
		commands.put("montagemCadastro", new BuildAddPersonCommand());
		commands.put("cadastroPessoa", new AddPersonCommand());
		commands.put("consultaPessoas", new ListPersonsCommand());
		commands.put("filtrarPessoas", new SearchPersonsCommand());
		commands.put("editarPessoa", new EditPersonCommand());
		commands.put("atualizaPessoa", new UpdatePersonCommand());
		commands.put("removerPessoa", new deletePersonCommand());
	}
	
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
    	String action = req.getParameter("action");
    	String nextPage = null;
    	
    	try {
	    	Command command = checkCommand(action);
	    	nextPage = command.execute(req);
		} catch (Exception e) {
			req.setAttribute("msgErro", e.getMessage());
		}
		RequestDispatcher dispatcher = req.getRequestDispatcher(nextPage);
		dispatcher.forward(req, res);
    }
    
    private Command checkCommand(String action) {
    	Command command = null;
    	for (String key : commands.keySet()) {
    		if (key.equalsIgnoreCase(action)) {
    			command = commands.get(key);
    		}
    	}
    	return command;
    }

}
