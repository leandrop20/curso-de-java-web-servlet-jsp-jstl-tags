package br.edu.devmedia.crud.validator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;

import br.edu.devmedia.crud.exception.ValidationException;
import br.edu.devmedia.crud.util.ConstantMessages;

public class DataValidator implements Validator {
	
	@Override
	public boolean validar(Map<String, Object> valores) throws ValidationException {
		String msgErro = "";
		for (String key : valores.keySet()) {
			String data = (String) valores.get(key);
			if (!data.equals("")) {
				try {
					if (data.length() < 10) {
						msgErro += ConstantMessages.MSG_ERR_CAMPO_DATA_MENOR.replace("?", key).concat("<br/>");
					}
					if (data.length() > 10) {
						msgErro += ConstantMessages.MSG_ERR_CAMPO_DATA_MAIOR.replace("?", key).concat("<br/>");
					}
					new SimpleDateFormat("dd/MM/yyyy").parse(data);
				} catch (ParseException e) {
					msgErro += ConstantMessages.MSG_ERR_CAMPO_INVALIDO.replace("?", key).concat("<br/>");
				}
			}
		}
		if (!msgErro.equals("")) {
			throw new ValidationException(msgErro);
		}
		return true;
	}
	
}
