package br.edu.devmedia.crud.validator;

import java.util.Map;

import br.edu.devmedia.crud.exception.ValidationException;

public interface Validator {

	public boolean validar(Map<String, Object> valores) throws ValidationException;
	
}
