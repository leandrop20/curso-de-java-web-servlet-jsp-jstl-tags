package br.edu.devmedia.crud.validator;

import java.util.Map;

import br.edu.devmedia.crud.exception.ValidationException;
import br.edu.devmedia.crud.util.ConstantMessages;
import br.edu.devmedia.crud.util.Util;

public class CPFValidator implements Validator {

	@Override
	public boolean validar(Map<String, Object> valores) throws ValidationException {
		String msgErro = "";
		for (String key : valores.keySet()) {
			String cpf = (String) valores.get(key);
			
			if (!cpf.equals("")) {
				if (!Util.isCPF(cpf)) {
					msgErro += ConstantMessages.MSG_ERR_CAMPO_INVALIDO.replace("?", key).concat("<br/>");
				}
				if (cpf.length() < 11) {
					msgErro += ConstantMessages.MSG_ERR_CAMPO_CPF_MENOR.replace("?", key).concat("<br/>");
				}
				if (cpf.length() > 11) {
					msgErro += ConstantMessages.MSG_ERR_CAMPO_CPF_MAIOR.replace("?", key).concat("<br/>");
				}
			} else {
				msgErro += ConstantMessages.MSG_ERR_CAMPO_OBRIGATORIO.replace("?", key).concat("<br/>");
			}
		}
		if (!msgErro.equals("")) {
			throw new ValidationException(msgErro);
		}
		return true;
	}
	
}
