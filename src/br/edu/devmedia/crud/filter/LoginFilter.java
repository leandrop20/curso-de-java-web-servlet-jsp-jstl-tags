package br.edu.devmedia.crud.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Servlet Filter implementation class LoginFilter
 */
@WebFilter("/*")
public class LoginFilter implements Filter {

	private static final String[] URLS_TO_EXCLUDE = {
		".css", ".js", ".png", ".jpg", ".gif", "login.jsp", "crud-web/"
	};
	
	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		//
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = ((HttpServletRequest) request);
		String uri = httpRequest.getRequestURI();
		
		if (!isURIToExclude(uri, httpRequest)) {
			HttpSession session = httpRequest.getSession();
			if (session.getAttribute("user") == null) {
				request.setAttribute("msgErro", "Acesso negado!");
				request.getRequestDispatcher("login.jsp").forward(request, response);;
			} else {
				chain.doFilter(request, response);
			}
		} else {
			chain.doFilter(request, response);
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		//
	}
	
	private boolean isURIToExclude(String uri, HttpServletRequest httpRequest)
	{
		boolean out = false;
		for (String url : URLS_TO_EXCLUDE) {
			if (uri != null && uri.endsWith(url)) {
				out = true;
			}
			if (uri != null && uri.endsWith("main")
					&& (httpRequest.getParameter("action") != null
					&& httpRequest.getParameter("action").equals("login"))) {
				out = true;
			}
		}
		return out;
	}

}
