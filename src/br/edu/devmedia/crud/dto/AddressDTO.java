package br.edu.devmedia.crud.dto;

import java.io.Serializable;

public class AddressDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String logradouro;
	private CityDTO city;
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getLogradouro() {
		return logradouro;
	}
	
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	
	public CityDTO getCity() {
		return city;
	}
	
	public void setCity(CityDTO city) {
		this.city = city;
	}
	
}
