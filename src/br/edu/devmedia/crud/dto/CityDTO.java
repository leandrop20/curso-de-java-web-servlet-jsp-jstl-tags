package br.edu.devmedia.crud.dto;

import java.io.Serializable;

public class CityDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String description;
	private StateDTO state;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public StateDTO getState() {
		return state;
	}
	public void setState(StateDTO state) {
		this.state = state;
	} 
}
