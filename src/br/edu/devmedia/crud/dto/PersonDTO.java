package br.edu.devmedia.crud.dto;

import java.io.Serializable;
import java.util.List;

public class PersonDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String name;
	private String cpf;
	private String dtBirth;
	private char sex; 
	private List<MusicalPreferenceDTO> musicalPreference;
	private String minBio;
	private AddressDTO address;
	
	public PersonDTO() {
		
	}
	
	public PersonDTO(String name, String cpf) {
		this.name = name;
		this.cpf = cpf;
	}

	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getCpf() {
		return cpf;
	}
	
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	public String getDtBirth() {
		return dtBirth;
	}
	
	public void setDtBirth(String dtBirth) {
		this.dtBirth = dtBirth;
	}
	
	public char getSex() {
		return sex;
	}
	
	public void setSex(char sex) {
		this.sex = sex;
	}
	
	public List<MusicalPreferenceDTO> getMusicalPreference() {
		return musicalPreference;
	}
	
	public void setMusicalPreference(List<MusicalPreferenceDTO> musicalPreference) {
		this.musicalPreference = musicalPreference;
	}
	
	public String getMinBio() {
		return minBio;
	}
	
	public void setMinBio(String minBio) {
		this.minBio = minBio;
	}
	
	public AddressDTO getAddress() {
		return address;
	}
	
	public void setAddress(AddressDTO address) {
		this.address = address;
	}
	
}
