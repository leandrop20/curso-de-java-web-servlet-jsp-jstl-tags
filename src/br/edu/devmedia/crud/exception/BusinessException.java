package br.edu.devmedia.crud.exception;

public class BusinessException extends Exception {
	private static final long serialVersionUID = 1L;

	public BusinessException(Exception e) {
		super(e.getMessage());
	}
	
	public BusinessException(String msg) {
		super(msg);
	}
	
}
