package br.edu.devmedia.crud.exception;

public class PersistenceException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public PersistenceException(String error) {
		super(error);
	}
	
	public PersistenceException(Exception e) {
		super(e.getMessage());
	}
	
	public PersistenceException(String error, Exception e) {
		super(error, e);
	}

}
