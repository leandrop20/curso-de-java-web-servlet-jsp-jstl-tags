package br.edu.devmedia.crud.decorator;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.displaytag.decorator.TableDecorator;

import br.edu.devmedia.crud.dto.MusicalPreferenceDTO;
import br.edu.devmedia.crud.dto.PersonDTO;

public class PersonDecorator extends TableDecorator {
	
	public String getName() {
		PersonDTO personDTO = (PersonDTO) getCurrentRowObject();
		return personDTO.getName() + " *";
	}
	
	public String getCpf() {
		PersonDTO personDTO = (PersonDTO) getCurrentRowObject();
		String cpf = personDTO.getCpf();
		return cpf.substring(0, 3) + "." +
			   cpf.substring(3, 6) + "." +
			   cpf.substring(6, 9) + "-" +
			   cpf.substring(9, 11);
	}
	
	public String getDtBirth() {
		PersonDTO personDTO = (PersonDTO) getCurrentRowObject();
		String dtBirth = personDTO.getDtBirth();
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		Date dateFormat = null;
		try {
			dateFormat = df.parse(dtBirth);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		return df.format(dateFormat);
	}
	
	public String getEdit() {
		PersonDTO personDTO = (PersonDTO) getCurrentRowObject();
		StringBuilder edit = new StringBuilder();
		edit.append("<a href='main?action=editarPessoa&id=")
			.append(personDTO.getId())
			.append("' title='Editar'>")
			.append("<img alt='Edi��o de Pessoa' src='img/edit.png' /></a>");
		return edit.toString();
	}
	
	public String getDelete() {
		PersonDTO personDTO = (PersonDTO) getCurrentRowObject();
		StringBuilder delete = new StringBuilder();
		delete.append("<a href='main?action=removerPessoa&id=")
			  .append(personDTO.getId())
			  .append("' title='Deletar'>")
			  .append("<img alt='Remo��o de Pessoa' src='img/delete.png' /></a>");
		return delete.toString();
	}
	
	public String getMusicalPreferences() {
		PersonDTO personDTO = (PersonDTO) getCurrentRowObject();
		
		StringBuilder prefs = new StringBuilder();
		if (personDTO.getMusicalPreference() != null && !personDTO.getMusicalPreference().isEmpty()) {
			for (MusicalPreferenceDTO p : personDTO.getMusicalPreference()) {
				prefs.append("[").append(p.getDescription()).append("]");
			}
		} else {
			prefs.append("Sem prefer�ncias!");
		}
		
		return prefs.toString();
	}
	
}
