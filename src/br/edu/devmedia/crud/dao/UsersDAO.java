package br.edu.devmedia.crud.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.PreparedStatement;

import br.edu.devmedia.crud.dto.UserDTO;
import br.edu.devmedia.crud.exception.PersistenceException;
import br.edu.devmedia.crud.util.ConnectionUtil;

public class UsersDAO {

	public boolean authUser(UserDTO userDTO) throws PersistenceException {
		try {
			Connection con = ConnectionUtil.getConnection();
			
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM users ");
			sql.append(" WHERE login = ? AND password = ?");
			
			PreparedStatement statement = (PreparedStatement) con.prepareStatement(sql.toString());
			statement.setString(1, userDTO.getLogin());
			statement.setString(2, userDTO.getPassword());
			
			ResultSet result = statement.executeQuery();
			return result.next();
		} catch (ClassNotFoundException | SQLException e) {
			throw new PersistenceException(e);
		}
	}
	
}
