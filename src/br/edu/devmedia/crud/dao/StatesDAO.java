package br.edu.devmedia.crud.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.PreparedStatement;

import br.edu.devmedia.crud.dto.StateDTO;
import br.edu.devmedia.crud.exception.PersistenceException;
import br.edu.devmedia.crud.util.ConnectionUtil;

public class StatesDAO {
	
	/**
	 * 
	 * @return
	 * @throws PersistenceException
	 */
	public List<StateDTO> getList() throws PersistenceException
	{
		List<StateDTO> stateDTOs = null;
		Connection conn = null;
		
		try {
			conn = ConnectionUtil.getConnection();
			
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM states");
			
			PreparedStatement statement = (PreparedStatement) conn.prepareStatement(sql.toString());
			
			ResultSet resultSet = statement.executeQuery();
			stateDTOs = new ArrayList<StateDTO>();
			StateDTO stateDTO;
			while (resultSet.next()) {
				stateDTO = new StateDTO();
				stateDTO.setId(resultSet.getInt("id"));
				stateDTO.setInitial(resultSet.getString("initials"));
				stateDTO.setDescription(resultSet.getString("description"));
				
				stateDTOs.add(stateDTO);
			}
		} catch (ClassNotFoundException | SQLException e) {
			throw new PersistenceException(e);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return stateDTOs;
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 * @throws PersistenceException
	 */
	public StateDTO getByID(Integer id) throws PersistenceException
	{
		StateDTO stateDTO = null;
		Connection conn = null;
		
		try {
			conn = ConnectionUtil.getConnection();
			
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM states WHERE id=?");
			
			PreparedStatement statement = (PreparedStatement) conn.prepareStatement(sql.toString());
			statement.setInt(1, id);
			
			ResultSet resultSet = statement.executeQuery();
			if (resultSet.first()) {
				stateDTO = new StateDTO();
				stateDTO.setId(resultSet.getInt("id"));
				stateDTO.setInitial(resultSet.getString("initials"));
				stateDTO.setDescription(resultSet.getString("description"));
			}
		} catch (ClassNotFoundException | SQLException e) {
			throw new PersistenceException(e);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return stateDTO;
	}
	
}
