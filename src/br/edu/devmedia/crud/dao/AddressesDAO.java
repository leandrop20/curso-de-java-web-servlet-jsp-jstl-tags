package br.edu.devmedia.crud.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mysql.jdbc.PreparedStatement;

import br.edu.devmedia.crud.dto.AddressDTO;
import br.edu.devmedia.crud.exception.PersistenceException;
import br.edu.devmedia.crud.util.ConnectionUtil;

public class AddressesDAO {
	
	private CitiesDAO citiesDAO;

	/**
	 * 
	 * @param id
	 * @return
	 * @throws PersistenceException
	 */
	public AddressDTO getByID(Integer id) throws PersistenceException
	{
		AddressDTO addressDTO = null;
		Connection conn = null;
		
		try {
			conn = ConnectionUtil.getConnection();
			
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM addresses WHERE id=?");
			
			PreparedStatement statement = (PreparedStatement) conn.prepareStatement(sql.toString());
			statement.setInt(1, id);
			
			ResultSet resultSet = statement.executeQuery();
			if (resultSet.first()) {
				addressDTO = new AddressDTO();
				addressDTO.setId(resultSet.getInt("id"));
				addressDTO.setLogradouro(resultSet.getString("logradouro"));
				
				citiesDAO = new CitiesDAO();
				addressDTO.setCity(citiesDAO.getByID(resultSet.getInt("city_id")));
			}
		} catch (ClassNotFoundException | SQLException e) {
			throw new PersistenceException(e);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return addressDTO;
	}
	
	/**
	 * 
	 * @param addressDTO
	 * @return
	 * @throws PersistenceException
	 */
	public Integer add(AddressDTO addressDTO) throws PersistenceException
	{
		Integer addressID = null;
		Connection conn = null;
		try {
			conn = ConnectionUtil.getConnection();
			
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO addresses(city_id, logradouro)");
			sql.append(" VALUES(?, ?)");
			
			PreparedStatement statement = (PreparedStatement) conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			statement.setInt(1, addressDTO.getCity().getId());
			statement.setString(2, addressDTO.getLogradouro());
			statement.executeUpdate();
			
			ResultSet resultSet =  statement.getGeneratedKeys();
			if (resultSet.first()) {
				addressID = resultSet.getInt(1);
			}
			return addressID;
		} catch (ClassNotFoundException | SQLException e) {
			throw new PersistenceException(e);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 
	 * @param addressDTO
	 * @param id
	 * @throws PersistenceException 
	 */
	public void edit(AddressDTO addressDTO) throws PersistenceException
	{
		Connection conn = null;
		
		try {
			conn = ConnectionUtil.getConnection();
			
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE addresses SET");
			sql.append(" city_id=?, logradouro=?");
			sql.append(" WHERE id=?");
			
			PreparedStatement statement = (PreparedStatement) conn.prepareStatement(sql.toString());
			statement.setInt(1, addressDTO.getCity().getId());
			statement.setString(2, addressDTO.getLogradouro());
			statement.setInt(3, addressDTO.getId());
			
			statement.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			throw new PersistenceException(e);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 
	 * @param id
	 * @throws PersistenceException
	 */
	public void delete(Integer id) throws PersistenceException
	{
		Connection conn = null;
		try {
			conn = ConnectionUtil.getConnection();
			
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM addresses WHERE id = ?");
			
			PreparedStatement statement = (PreparedStatement) conn.prepareStatement(sql.toString());
			statement.setInt(1, id);
			
			statement.execute();
		} catch (ClassNotFoundException | SQLException e) {
			throw new PersistenceException(e);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
}
