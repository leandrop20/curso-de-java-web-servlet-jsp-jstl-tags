package br.edu.devmedia.crud.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.PreparedStatement;

import br.edu.devmedia.crud.dto.CityDTO;
import br.edu.devmedia.crud.exception.PersistenceException;
import br.edu.devmedia.crud.util.ConnectionUtil;

public class CitiesDAO {
	
	private StatesDAO statesDAO;
	
	/**
	 * 
	 * @return
	 * @throws PersistenceException
	 */
	public List<CityDTO> getListByStateID(Integer id) throws PersistenceException
	{
		List<CityDTO> cityDTOs = null;
		Connection conn = null;
		
		try {
			conn = ConnectionUtil.getConnection();
			
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM cities WHERE state_id=?");
			
			PreparedStatement statement = (PreparedStatement) conn.prepareStatement(sql.toString());
			statement.setInt(1, id);
			
			ResultSet resultSet = statement.executeQuery();
			cityDTOs = new ArrayList<CityDTO>();
			CityDTO cityDTO;
			while (resultSet.next()) {
				cityDTO = new CityDTO();
				cityDTO.setId(resultSet.getInt("id"));
				cityDTO.setDescription(resultSet.getString("description"));
				
				cityDTOs.add(cityDTO);
			}
		} catch (ClassNotFoundException | SQLException e) {
			throw new PersistenceException(e);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return cityDTOs;
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 * @throws PersistenceException
	 */
	public CityDTO getByStateID(Integer id) throws PersistenceException
	{
		CityDTO cityDTO = null;
		Connection conn = null;
		
		try {
			conn = ConnectionUtil.getConnection();
			
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM cities WHERE state_id=?");
			
			PreparedStatement statement = (PreparedStatement) conn.prepareStatement(sql.toString());
			statement.setInt(1, id);
			
			ResultSet resultSet = statement.executeQuery();
			if (resultSet.first()) {
				cityDTO = new CityDTO();
				cityDTO.setId(resultSet.getInt("id"));
				cityDTO.setDescription(resultSet.getString("description"));
				
				statesDAO = new StatesDAO();
				cityDTO.setState(statesDAO.getByID(resultSet.getInt("id")));
			}
		} catch (ClassNotFoundException | SQLException e) {
			throw new PersistenceException(e);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return cityDTO;
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 * @throws PersistenceException
	 */
	public CityDTO getByID(Integer id) throws PersistenceException
	{
		CityDTO cityDTO = null;
		Connection conn = null;
		
		try {
			conn = ConnectionUtil.getConnection();
			
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM cities WHERE id=?");
			
			PreparedStatement statement = (PreparedStatement) conn.prepareStatement(sql.toString());
			statement.setInt(1, id);
			
			ResultSet resultSet = statement.executeQuery();
			if (resultSet.first()) {
				cityDTO = new CityDTO();
				cityDTO.setId(resultSet.getInt("id"));
				cityDTO.setDescription(resultSet.getString("description"));
				
				statesDAO = new StatesDAO();
				cityDTO.setState(statesDAO.getByID(resultSet.getInt("id")));
			}
		} catch (ClassNotFoundException | SQLException e) {
			throw new PersistenceException(e);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return cityDTO;
	}
	
}
