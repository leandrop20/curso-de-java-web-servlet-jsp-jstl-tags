package br.edu.devmedia.crud.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.PreparedStatement;

import br.edu.devmedia.crud.dto.AddressDTO;
import br.edu.devmedia.crud.dto.CityDTO;
import br.edu.devmedia.crud.dto.MusicalPreferenceDTO;
import br.edu.devmedia.crud.dto.PersonDTO;
import br.edu.devmedia.crud.dto.StateDTO;
import br.edu.devmedia.crud.exception.PersistenceException;
import br.edu.devmedia.crud.util.ConnectionUtil;

public class PersonsDAO {
	
	private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	private AddressesDAO addressesDAO;
	private MusicalPreferencesDAO musicalPreferencesDAO;
	
	public PersonsDAO()
	{
		addressesDAO = new AddressesDAO();
		musicalPreferencesDAO = new MusicalPreferencesDAO();
	}
	
	/**
	 * 
	 * @return
	 * @throws PersistenceException
	 */
	public List<PersonDTO> getList() throws PersistenceException
	{
		List<PersonDTO> listaPessoas = new ArrayList<>();
		Connection conn = null;
		try {
			conn = ConnectionUtil.getConnection();
			
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT P.id, P.name, P.cpf, P.dt_birth, P.sex");
			sql.append(", A.logradouro, C.description AS city, S.description AS state, S.initials AS uf");
			sql.append(" FROM persons P");
			sql.append(" INNER JOIN addresses A");
			sql.append(" ON A.id = P.address_id");
			sql.append(" INNER JOIN cities C");
			sql.append(" ON C.id = A.city_id");
			sql.append(" INNER JOIN states S");
			sql.append(" ON S.id = C.state_id");
			sql.append(" ORDER BY P.id");
			
			PreparedStatement statement = (PreparedStatement) conn.prepareStatement(sql.toString());
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				PersonDTO personDTO = new PersonDTO();
				personDTO.setId(resultSet.getInt("id"));
				personDTO.setName(resultSet.getString("name"));
				personDTO.setCpf(resultSet.getString("cpf"));
				personDTO.setSex(resultSet.getString("sex").charAt(0));
				personDTO.setDtBirth(dateFormat.format(resultSet.getDate("dt_birth")));
				
				AddressDTO addressDTO = new AddressDTO();
				addressDTO.setLogradouro(resultSet.getString("logradouro"));
				
				CityDTO cityDTO = new CityDTO();
				cityDTO.setDescription(resultSet.getString("city"));
				
				StateDTO stateDTO = new StateDTO();
				stateDTO.setDescription(resultSet.getString("state"));
				stateDTO.setInitial(resultSet.getString("uf"));
				
				addressDTO.setCity(cityDTO);
				cityDTO.setState(stateDTO);
				personDTO.setAddress(addressDTO);
				
				personDTO.setMusicalPreference(musicalPreferencesDAO.getListByPersonID(personDTO.getId()));
				
				listaPessoas.add(personDTO);
			}
		} catch (ClassNotFoundException | SQLException e) {
			throw new PersistenceException(e);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return listaPessoas;
	}
	
	/**
	 * 
	 * @param personDTO
	 * @return
	 * @throws PersistenceException
	 */
	public List<PersonDTO> getListBySearch(PersonDTO personDTO) throws PersistenceException
	{
		List<PersonDTO> persons = null;
		Connection conn = null;
		
		try {
			conn = ConnectionUtil.getConnection();
			
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM persons");
			sql.append(" INNER JOIN addresses A");
			sql.append(" ON A.id = persons.address_id");
			sql.append(" INNER JOIN cities C");
			sql.append(" ON C.id = A.city_id");
			sql.append(" INNER JOIN states S");
			sql.append(" ON S.id = C.state_id");
			if (personDTO.getMusicalPreference() != null) {
				sql.append(" INNER JOIN persons_musical_preferences pmp");
				sql.append(" ON pmp.person_id = persons.id");
				sql.append(" INNER JOIN musical_preferences mp");
				sql.append(" ON mp.id = pmp.musical_preference_id");
			}
			sql.append(" WHERE 1=1");
			if (personDTO.getName() != null && !personDTO.getName().equals("")) {
				sql.append(" AND name LIKE ?");
			}
			if (personDTO.getCpf() != null && !personDTO.getCpf().equals("")) {
				sql.append(" AND cpf LIKE ?");
			}
			if (personDTO.getDtBirth() != null && !personDTO.getDtBirth().equals("")) {
				sql.append(" AND dt_birth=?");
			}
			if (personDTO.getSex() != 0 && personDTO.getSex() != ' ') {
				sql.append(" AND sex=?");
			}
			if (personDTO.getAddress().getCity().getId() != 0) {
				sql.append(" AND C.id=?");
			}
			if (personDTO.getAddress().getCity().getState().getId() != 0) {
				sql.append(" AND S.id=?");
			}
			if (personDTO.getAddress().getLogradouro() != null && !personDTO.getAddress().getLogradouro().equals("")) {
				sql.append(" AND A.logradouro LIKE ?");
			}
			if (personDTO.getMusicalPreference() != null) {
				String mpsSelected = "";
				
				for (int i=0; i<personDTO.getMusicalPreference().size();i++) {
					mpsSelected += "?,";
				}
				mpsSelected = mpsSelected.substring(0, mpsSelected.length()-1);
				sql.append(" AND mp.id IN (" + mpsSelected + ") GROUP BY persons.id");
			}
			
			Integer i = 0;
			PreparedStatement statement = (PreparedStatement) conn.prepareStatement(sql.toString());
			if (personDTO.getName() != null && !personDTO.getName().equals("")) {
				statement.setString(++i, "%" + personDTO.getName() + "%");
			}
			if (personDTO.getCpf() != null && !personDTO.getCpf().equals("")) {
				statement.setString(++i, "%" + personDTO.getCpf() + "%");
			}
			if (personDTO.getDtBirth() != null && !personDTO.getDtBirth().equals("")) {
				java.sql.Date dtBirth = new java.sql.Date(dateFormat.parse(personDTO.getDtBirth()).getTime());
				statement.setDate(++i, dtBirth);
			}
			if (personDTO.getSex() != 0 && personDTO.getSex() != ' ') {
				statement.setString(++i, String.valueOf(personDTO.getSex()));
			}
			if (personDTO.getAddress().getCity().getId() != 0) {
				statement.setInt(++i, personDTO.getAddress().getCity().getId());
			}
			if (personDTO.getAddress().getCity().getState().getId() != 0) {
				statement.setInt(++i, personDTO.getAddress().getCity().getState().getId());
			}
			if (personDTO.getAddress().getLogradouro() != null && !personDTO.getAddress().getLogradouro().equals("")) {
				statement.setString(++i, "%" + personDTO.getAddress().getLogradouro() + "%");
			}
			if (personDTO.getMusicalPreference() != null) {
				for (MusicalPreferenceDTO mp : personDTO.getMusicalPreference()) {
					statement.setInt(++i, mp.getId());
				}
			}
			
			ResultSet resultSet = statement.executeQuery();
			persons = new ArrayList<PersonDTO>();
			while (resultSet.next()) {
				PersonDTO person = new PersonDTO();
				person.setId(resultSet.getInt("id"));
				person.setName(resultSet.getString("name"));
				person.setCpf(resultSet.getString("cpf"));
				person.setSex(resultSet.getString("sex").charAt(0));
				person.setDtBirth(dateFormat.format(resultSet.getDate("dt_birth")));
				
				AddressDTO addressDTO = new AddressDTO();
				addressDTO.setLogradouro(resultSet.getString("logradouro"));
				
				CityDTO cityDTO = new CityDTO();
				cityDTO.setDescription(resultSet.getString("C.description"));
				addressDTO.setCity(cityDTO);
				
				StateDTO stateDTO = new StateDTO();
				stateDTO.setInitial(resultSet.getString("S.initials"));
				cityDTO.setState(stateDTO);
				
				person.setAddress(addressDTO);
				
				person.setMusicalPreference(musicalPreferencesDAO.getListByPersonID(person.getId()));
				
				persons.add(person);
			}
			
		} catch (ClassNotFoundException | SQLException | ParseException e) {
			throw new PersistenceException(e);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return persons;
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 * @throws PersistenceException
	 */
	public PersonDTO getByID(Integer id) throws PersistenceException
	{
		PersonDTO personDTO = null;
		Connection conn = null;
		try {
			conn = ConnectionUtil.getConnection();
			
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT P.id, P.name, P.cpf, P.dt_birth, P.sex, P.mini_bio");
			sql.append(", A.id AS address_id, A.logradouro, C.id AS city_id, C.description AS city, S.id AS state_id, S.description AS state");
			sql.append(" FROM persons P");
			sql.append(" INNER JOIN addresses A");
			sql.append(" ON A.id = P.address_id");
			sql.append(" INNER JOIN cities C");
			sql.append(" ON C.id = A.city_id");
			sql.append(" INNER JOIN states S");
			sql.append(" ON S.id = C.state_id");
			sql.append(" WHERE P.id = ?");
			
			PreparedStatement statement = (PreparedStatement) conn.prepareStatement(sql.toString());
			statement.setInt(1, id);
			
			ResultSet resultSet = statement.executeQuery();
			if (resultSet.first()) {
				personDTO = new PersonDTO();
				personDTO.setId(resultSet.getInt("id"));
				personDTO.setName(resultSet.getString("name"));
				personDTO.setCpf(resultSet.getString("cpf"));
				personDTO.setSex(resultSet.getString("sex").charAt(0));
				personDTO.setDtBirth(dateFormat.format(resultSet.getDate("dt_birth")));
				personDTO.setMinBio(resultSet.getString("mini_bio"));
				
				AddressDTO addressDTO = new AddressDTO();
				addressDTO.setId(resultSet.getInt("address_id"));
				addressDTO.setLogradouro(resultSet.getString("logradouro"));
				
				CityDTO cityDTO = new CityDTO();
				cityDTO.setId(resultSet.getInt("city_id"));
				cityDTO.setDescription(resultSet.getString("city"));
				
				StateDTO stateDTO = new StateDTO();
				stateDTO.setId(resultSet.getInt("state_id"));
				stateDTO.setDescription(resultSet.getString("state"));
				
				addressDTO.setCity(cityDTO);
				cityDTO.setState(stateDTO);
				personDTO.setAddress(addressDTO);
				personDTO.setMusicalPreference(musicalPreferencesDAO.getListByPersonID(personDTO.getId()));
			}
		} catch (ClassNotFoundException | SQLException e) {
			throw new PersistenceException(e);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return personDTO;
	}
	
	/**
	 * 
	 * @param personDTO
	 * @throws PersistenceException
	 */
	public void add(PersonDTO personDTO) throws PersistenceException
	{
		Connection conn = null;
		try {
			Integer person_id = null;
			Integer address_id = addressesDAO.add(personDTO.getAddress());
			
			conn = ConnectionUtil.getConnection();
			
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO persons (name, cpf, dt_birth, sex, mini_bio, address_id)");
			sql.append(" VALUES(?, ?, ?, ?, ?, ?)");
			
			PreparedStatement statement = (PreparedStatement) conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			statement.setString(1,  personDTO.getName());
			statement.setString(2, personDTO.getCpf());
			
			java.sql.Date dtBirth = new java.sql.Date(dateFormat.parse(personDTO.getDtBirth()).getTime());
			statement.setDate(3, dtBirth);
			
			statement.setString(4, String.valueOf(personDTO.getSex()));
			statement.setString(5, personDTO.getMinBio());
			statement.setInt(6, address_id);
			
			statement.executeUpdate();
			
			ResultSet resultSet = statement.getGeneratedKeys();
			if (resultSet.first()) {
				person_id = resultSet.getInt(1);
				musicalPreferencesDAO.addInPerson(personDTO.getMusicalPreference(), person_id);
			}
		} catch (ClassNotFoundException | SQLException | ParseException e) {
			throw new PersistenceException(e);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 
	 * @param personDTO
	 * @throws PersistenceException
	 */
	public void edit(PersonDTO personDTO) throws PersistenceException
	{
		Connection conn = null;
		
		try {
			conn = ConnectionUtil.getConnection();
			
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE persons SET");
			sql.append(" name=?, cpf=?, dt_birth=?, sex=?, mini_bio=?");
			sql.append(" WHERE id=?");
			
			PreparedStatement statement = (PreparedStatement) conn.prepareStatement(sql.toString());
			statement.setString(1, personDTO.getName());
			statement.setString(2, personDTO.getCpf());
			
			java.sql.Date dtBirth = new java.sql.Date(dateFormat.parse(personDTO.getDtBirth()).getTime());
			statement.setDate(3, dtBirth);
			
			statement.setString(4, String.valueOf(personDTO.getSex()));
			statement.setString(5, personDTO.getMinBio());
			statement.setInt(6, personDTO.getId());
			
			statement.executeUpdate();
			
			musicalPreferencesDAO.deleteInPerson(personDTO.getId());
			musicalPreferencesDAO.addInPerson(personDTO.getMusicalPreference(), personDTO.getId());
			
			addressesDAO.edit(personDTO.getAddress());
		} catch (ClassNotFoundException | SQLException | ParseException e) {
			throw new PersistenceException(e);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 
	 * @param personDTO
	 * @throws PersistenceException
	 */
	public void delete(Integer id) throws PersistenceException
	{
		Connection conn = null;
		try {
			PersonDTO personDTO = getByID(id);
			
			if (personDTO.getMusicalPreference() != null && !personDTO.getMusicalPreference().isEmpty()) {
				musicalPreferencesDAO.deleteInPerson(personDTO.getId());
			}
			if (personDTO.getAddress() != null && personDTO.getAddress().getId() != null) {
				addressesDAO.delete(personDTO.getAddress().getId());
			}
			
			conn = ConnectionUtil.getConnection();
			
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM persons WHERE id = ?");
			
			PreparedStatement statement = (PreparedStatement) conn.prepareStatement(sql.toString());
			statement.setInt(1, personDTO.getId());
			
			statement.execute();
		} catch (ClassNotFoundException | SQLException e) {
			throw new PersistenceException(e);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
}
