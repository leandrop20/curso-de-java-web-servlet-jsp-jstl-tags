package br.edu.devmedia.crud.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.PreparedStatement;

import br.edu.devmedia.crud.dto.MusicalPreferenceDTO;
import br.edu.devmedia.crud.exception.PersistenceException;
import br.edu.devmedia.crud.util.ConnectionUtil;

public class MusicalPreferencesDAO {

	public List<MusicalPreferenceDTO> getList() throws PersistenceException
	{
		List<MusicalPreferenceDTO> musicalPreferenceDTOs = null;
		Connection conn = null;
		
		try {
			conn = ConnectionUtil.getConnection();
			
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM musical_preferences");
			
			PreparedStatement statement = (PreparedStatement) conn.prepareStatement(sql.toString());
			
			ResultSet resultSet = statement.executeQuery();
			musicalPreferenceDTOs = new ArrayList<MusicalPreferenceDTO>();
			MusicalPreferenceDTO musicalPreferenceDTO;
			while (resultSet.next()) {
				musicalPreferenceDTO = new MusicalPreferenceDTO();
				musicalPreferenceDTO.setId(resultSet.getInt("id"));
				musicalPreferenceDTO.setDescription(resultSet.getString("description"));
				
				musicalPreferenceDTOs.add(musicalPreferenceDTO);
			}
			
		} catch (ClassNotFoundException | SQLException e) {
			throw new PersistenceException(e);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return musicalPreferenceDTOs;
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 * @throws PersistenceException
	 */
	public List<MusicalPreferenceDTO> getListByPersonID(Integer id) throws PersistenceException
	{
		List<MusicalPreferenceDTO> musicalPreferenceDTOs = null;
		Connection conn = null;
		
		try {
			conn = ConnectionUtil.getConnection();
			
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT MP.id AS id, MP.description AS description FROM musical_preferences MP");
			sql.append(" INNER JOIN persons_musical_preferences PMP");
			sql.append(" ON PMP.musical_preference_id = MP.id");
			sql.append(" INNER JOIN persons P");
			sql.append(" ON P.id = PMP.person_id");
			sql.append(" WHERE P.id=?");
			
			PreparedStatement statement = (PreparedStatement) conn.prepareStatement(sql.toString());
			statement.setInt(1, id);
			
			ResultSet resultSet = statement.executeQuery();
			musicalPreferenceDTOs = new ArrayList<MusicalPreferenceDTO>();
			MusicalPreferenceDTO musicalPreferenceDTO;
			while (resultSet.next()) {
				musicalPreferenceDTO = new MusicalPreferenceDTO();
				musicalPreferenceDTO.setId(resultSet.getInt("id"));
				musicalPreferenceDTO.setDescription(resultSet.getString("description"));
				
				musicalPreferenceDTOs.add(musicalPreferenceDTO);
			}
			
		} catch (ClassNotFoundException | SQLException e) {
			throw new PersistenceException(e);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return musicalPreferenceDTOs;
	}
	
	public void addInPerson(List<MusicalPreferenceDTO> list, Integer id) throws PersistenceException
	{
		Connection conn = null;
		
		try {
			conn = ConnectionUtil.getConnection();
			
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO persons_musical_preferences");
			sql.append(" VALUES(?, ?)");
			
			for (MusicalPreferenceDTO mp : list) {
				PreparedStatement statement = (PreparedStatement) conn.prepareStatement(sql.toString());
				statement.setInt(1, id);
				statement.setInt(2, mp.getId());
				
				statement.execute();
			}
		} catch (ClassNotFoundException | SQLException e) {
			throw new PersistenceException(e);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 
	 * @param person_id
	 * @param prefs
	 * @throws PersistenceException 
	 */
	public void deleteInPerson(Integer id) throws PersistenceException
	{
		Connection conn = null;
		try {
			conn = ConnectionUtil.getConnection();
			
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM persons_musical_preferences");
			sql.append(" WHERE person_id = ?");
			
			PreparedStatement statement = (PreparedStatement) conn.prepareStatement(sql.toString());
			statement.setInt(1, id);
			
			statement.execute();
		} catch (ClassNotFoundException | SQLException e) {
			throw new PersistenceException(e);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
}
