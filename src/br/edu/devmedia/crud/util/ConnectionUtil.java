package br.edu.devmedia.crud.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class ConnectionUtil {

	private static ResourceBundle configDB = ResourceBundle.getBundle(Constants.CONNECTION_DB_PROPERTIES);
	
	public static Connection getConnection() throws ClassNotFoundException, SQLException {
		Class.forName(configDB.getString(Constants.CONNECTION_DB_DRIVER));
		return DriverManager.getConnection(
				configDB.getString(Constants.CONNECTION_DB_URL),
				configDB.getString(Constants.CONNECTION_DB_USER),
				configDB.getString(Constants.CONNECTION_DB_PASSWORD)
		);
	}
	
}