package br.edu.devmedia.crud.util;

public class Constants {

	public static final String CONNECTION_DB_PROPERTIES = "br.edu.devmedia.crud.util.config_db";
	public static final String CONNECTION_DB_DRIVER = "connection.driver.mysql";
	public static final String CONNECTION_DB_URL = "connection.url";
	public static final String CONNECTION_DB_USER = "connection.user";
	public static final String CONNECTION_DB_PASSWORD = "connection.password";
	
}