package br.edu.devmedia.crud.util;

public class ConstantMessages {

	public static final String MSG_ERR_CAMPO_OBRIGATORIO = "Campo ? obrigatório!";
	public static final String MSG_ERR_CAMPO_INVALIDO = "Campo ? inválido!";
	public static final String MSG_ERR_USER_INVALIDO = "Usuário ou Senha Inválidos!";
	
	public static final String MSG_ERR_PERSON_INVALIDO = "Dados da Pessoa Inválidos!";
	
	public static final String MSG_ERR_CAMPO_CPF_MAIOR = "Campo ? com mais de 11 caractes!";
	public static final String MSG_ERR_CAMPO_CPF_MENOR = "Campo ? com menos de 11 caractes!";
	
	public static final String MSG_ERR_CAMPO_DATA_MAIOR = "Campo ? com mais de 10 caractes!";
	public static final String MSG_ERR_CAMPO_DATA_MENOR = "Campo ? com menos de 10 caractes!";
	
	public static final String MSG_SUC_ADD_PERSON = "Cadastro de pessoa efetuado com sucesso!";
	public static final String MSG_SUC_EDIT_PERSON = "Cadastro de pessoa atualizado com sucesso!";
	
}
